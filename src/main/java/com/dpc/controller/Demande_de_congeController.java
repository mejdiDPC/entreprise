package com.dpc.controller;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.dpc.domain.Demande_de_conge;
import com.dpc.domain.Roles;
import com.dpc.domain.Type;
import com.dpc.domain.Utilisateur;
import com.dpc.dto.Demande_de_congeDto;
import com.dpc.dto.TypeDto;
import com.dpc.repository.Demande_de_congerepository;
import com.dpc.repository.Utilisateurrepository;
import com.dpc.service.Demande_de_congeservice;

@RestController
public class Demande_de_congeController extends Maincontroller {

	
	@Autowired
	private Demande_de_congeservice demande_de_congeservice ; 
	@Autowired
	Demande_de_congerepository demande_de_congerepository ;

	//fonction ajouter demande de conge
	@PostMapping(value="/ajouterDemande_de_conge")
	public void saveDemande_de_conge(@RequestBody Demande_de_conge demande_de_conge)
	{
		demande_de_congeservice.saveDemande_de_conge(demande_de_conge);
	}

	
	
	 // fct qui affiche les types
	  @RequestMapping(value = "/getDemandedeconge", method = RequestMethod.GET)
	  public Collection<Demande_de_conge> getDemandedeconge() {   
		   return this.demande_de_congeservice.getDemandedeconge();
	  }
		
		 // fct qui affiche les types
	   @RequestMapping(value = "/getAllDemandedecongedto", method = RequestMethod.GET)
	   public Collection<Demande_de_congeDto> getDemandedecongedto() {   
		   return this.demande_de_congeservice.getDemandedeconge().stream().map(demande_de_conge->convertDemande_de_congeToDto(demande_de_conge))
	   			.collect(Collectors.toList());

	   }
	   //fct pour modifier le contenue de l'entité utilisateur
       @GetMapping(value = "/updateDemandeconge")
       public void updateDemandeconge(@RequestBody Demande_de_conge demande_de_conge) { //Cette annotation demande à Spring que le JSON contenu dans la partie body de la requête HTTP soit converti en objet Java
           demande_de_congeservice.update(demande_de_conge);
	} 
	    
     //une fct qui supprime un employes a travers son motif
       @GetMapping(value = "/deleteDemandeByMotif/{motif}")
       public Demande_de_conge getDemandeMotif( String motif) {
       	System.out.println(motif);
       	this.demande_de_congeservice.deleteMotif(motif);

           return null;
       }
     //fct qui affiche un Demandede conge a travers son id
       @GetMapping(value="/getDemandecongeId/{id}")
       public Demande_de_conge getDemandecongeId(@PathVariable long id) {
    	   return demande_de_congeservice.getDemandecongeId(id);
       }


       
}
