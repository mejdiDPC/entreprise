package com.dpc.controller;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;

import com.dpc.domain.Demande_de_conge;
import com.dpc.domain.Domaine_activite;
import com.dpc.domain.Entreprise;
import com.dpc.domain.Pointage_Emp;
import com.dpc.domain.Roles;
import com.dpc.domain.Type;
import com.dpc.domain.Type_contrat;
import com.dpc.domain.Utilisateur;
import com.dpc.dto.Demande_de_congeDto;
import com.dpc.dto.Domaine_activiteDto;
import com.dpc.dto.EntrepriseDto;
import com.dpc.dto.Pointage_EmpDto;
import com.dpc.dto.RolesDto;
import com.dpc.dto.TypeDto;
import com.dpc.dto.UtilisateurDto;
import com.dpc.dto.Type_contratDto;

public class Maincontroller {
	
@Autowired
protected ModelMapper modelMapper;


 

//les méthodes de conversion en DTO
protected EntrepriseDto convertEntrepriseToDto(Entreprise entreprise) {
EntrepriseDto entrepriseDto = modelMapper.map(entreprise, EntrepriseDto.class);
return entrepriseDto ;
}

//les méthodes de conversion en Entity	
protected Entreprise dtoToEntreprise(EntrepriseDto entrepriseDto) {
Entreprise entreprise = modelMapper.map(entrepriseDto, Entreprise.class);
return entreprise;
}



//les méthodes de conversion en DTO
protected Demande_de_congeDto convertDemande_de_congeToDto(Demande_de_conge demande_de_conge) {
Demande_de_congeDto demande_de_congeDto = modelMapper.map(demande_de_conge, Demande_de_congeDto.class);
return demande_de_congeDto ;
}

//les méthodes de conversion en Entity	
protected Demande_de_conge dtoToDemande_de_conge(Demande_de_congeDto demande_de_congeDto) {
Demande_de_conge demande_de_conge = modelMapper.map(demande_de_congeDto, Demande_de_conge.class);
return demande_de_conge;
}

//les méthodes de conversion en DTO
protected Domaine_activiteDto convertDomaine_activiteToDto(Domaine_activite domaine_activite) {
Domaine_activiteDto domaine_activiteDto = modelMapper.map(domaine_activite, Domaine_activiteDto.class);
return domaine_activiteDto ;
}

//les méthodes de conversion en Entity	
protected Domaine_activite dtoToDomaine_activite(Domaine_activiteDto domaine_activiteDto) {
Domaine_activite domaine_activite = modelMapper.map(domaine_activiteDto, Domaine_activite.class);
return domaine_activite;
}
//les méthodes de conversion en DTO
protected Pointage_EmpDto convertPointageToDto(Pointage_Emp pointage_Emp) {
Pointage_EmpDto pointage_EmpDto = modelMapper.map(pointage_Emp, Pointage_EmpDto.class);
return pointage_EmpDto ;
}

//les méthodes de conversion en Entity	
protected Pointage_Emp dtoToPointage(Pointage_EmpDto pointage_EmpDto) {
Pointage_Emp pointage_Emp = modelMapper.map(pointage_EmpDto, Pointage_Emp.class);
return pointage_Emp;
}

//les méthodes de conversion en DTO
protected RolesDto convertRoleToDto(Roles roles) {
RolesDto rolesDto = modelMapper.map(roles, RolesDto.class);
return rolesDto ;
}

//les méthodes de conversion en Entity	
protected Roles dtoToRole(RolesDto rolesDto) {
Roles roles = modelMapper.map(rolesDto, Roles.class);
return roles;
}

//les méthodes de conversion en DTO
protected Type_contratDto converttype_contratToDto(Type_contrat type_contrat) {
Type_contratDto type_contratDto = modelMapper.map(type_contrat, Type_contratDto.class);
return type_contratDto ;
}

//les méthodes de conversion en Entity	
protected Type_contrat dtoToType_contrat(Type_contratDto type_contratDto) {
Type_contrat type_contrat = modelMapper.map(type_contratDto, Type_contrat.class);
return type_contrat;
}

//les méthodes de conversion en DTO
protected TypeDto convertRoleToDto(Type type) {
TypeDto typeDto = modelMapper.map(type, TypeDto.class);
return typeDto ;
}

//les méthodes de conversion en Entity	
protected Type dtoToType(TypeDto typeDto) {
Type type = modelMapper.map(typeDto, Type.class);
return type;
}

//les méthodes de conversion en DTO

protected UtilisateurDto convertuserToDto(Utilisateur utilisateur) {
UtilisateurDto utilisateurDto = modelMapper.map(utilisateur, UtilisateurDto.class);
return utilisateurDto ;
}
//les méthodes de conversion en Entity	

protected Utilisateur dtoToUer(UtilisateurDto u) {
Utilisateur user  = modelMapper.map(u,Utilisateur.class);
return user;
}

}
