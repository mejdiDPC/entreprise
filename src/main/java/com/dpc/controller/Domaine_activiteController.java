package com.dpc.controller;

import java.util.Collection;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.dpc.controller.Maincontroller;

import com.dpc.domain.Domaine_activite;
import com.dpc.domain.Type;
import com.dpc.domain.Utilisateur;
import com.dpc.dto.Domaine_activiteDto;
import com.dpc.dto.UtilisateurDto;
import com.dpc.service.Domaine_activiteservice;

@RestController
public class Domaine_activiteController extends Maincontroller {

	
@Autowired
private Domaine_activiteservice domaine_activiteservice ; 



// fct qui affiche la liste de domaine d'activite dto
@RequestMapping(value = "/getAllDomaineDto", method = RequestMethod.GET)
public Collection<Domaine_activiteDto>getAllDomaineDto(){

	return this.domaine_activiteservice.getAllDomaine().stream().map(domaine_activite->convertDomaine_activiteToDto(domaine_activite))
			.collect(Collectors.toList());
}




// fct qui affiche les types
@RequestMapping(value = "/getAllDomaine", method = RequestMethod.GET)
public Collection<Domaine_activite> getAllDomaine() {   
  return this.domaine_activiteservice.getAllDomaine();
}




//fonction ajouter Domaine_activite
@PostMapping(value="/ajouterDomaine_activite")
public void saveDomaine_activite(@RequestBody Domaine_activite domaine_activite)
{
	domaine_activiteservice.saveDomaine_activite(domaine_activite);
}

//fct pour modifier le contenue de l'entité utilisateur
@GetMapping(value = "/updateDomaineactivite")
public void updateDomaineactivite(@RequestBody Domaine_activite domaine_activite) { //Cette annotation demande à Spring que le JSON contenu dans la partie body de la requête HTTP soit converti en objet Java
    domaine_activiteservice.update(domaine_activite);
} 
//fonction supprimer domaine activite  a travers son nom
@GetMapping(value="/deleteDomaineBynom/{nom}")
public Domaine_activite getDomaine(String nom) {
	System.out.println(nom);
	this.domaine_activiteservice.deleteNom(nom);
	return null ; 
}
}
