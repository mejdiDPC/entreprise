package com.dpc.controller;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.dpc.controller.Maincontroller;
import com.dpc.domain.Roles;
import com.dpc.domain.Type;
import com.dpc.domain.Type_contrat;
import com.dpc.domain.Utilisateur;
import com.dpc.dto.Type_contratDto;
import com.dpc.service.Type_contactservice;



@RestController
public class Type_contactController extends Maincontroller {

	@Autowired
	private Type_contactservice type_contactservice ; 




    // Fct ajouter un employes
    @PostMapping(value = "/AddAjoutcontrat")
    public void saveTypecontrat(@RequestBody Type_contrat type_contrat) {
    	type_contactservice.saveTypecontrat(type_contrat);
    }


    
	 // fct qui affiche les types de contrats
 @RequestMapping(value = "/getAlltypecontrat", method = RequestMethod.GET)
 public Collection<Type_contrat> getTypecontrat() {   
	   return this.type_contactservice.getTypecontrat();
 }
	
	
	
	
	 // fct qui affiche les types_contrat
   @RequestMapping(value = "/getAllType_contratdto", method = RequestMethod.GET)
   public Collection<Type_contratDto> getType_contratDto() {   

		return this.type_contactservice.getTypecontrat().stream().map(type_contrat->converttype_contratToDto(type_contrat))
   			.collect(Collectors.toList());
   }
	  //fct pour modifier le contenue de l'entité utilisateur
   @PostMapping(value = "/updateType_contact")
   public String updateType_contact(@RequestBody Type_contrat type_contrat) { //Cette annotation demande à Spring que le JSON contenu dans la partie body de la requête HTTP soit converti en objet Java
     type_contactservice.update(type_contrat);
     return "ok";
   }
   


 //une fct qui supprime un Type-contrat a travers son type
 @GetMapping(value = "/deleteTypecontrat/{type}")
 public Type_contrat getTypecontrat( String type) {
 	System.out.println(type);
 	this.type_contactservice.deleteType(type);

     return null;
 }
}
