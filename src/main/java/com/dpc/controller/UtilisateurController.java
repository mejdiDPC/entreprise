package com.dpc.controller;


import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.dpc.controller.Maincontroller;
import com.dpc.domain.Demande_de_conge;
import com.dpc.domain.Entreprise;
import com.dpc.domain.Type;
import com.dpc.domain.Utilisateur;
import com.dpc.dto.TypeDto;
import com.dpc.dto.UtilisateurDto;
import com.dpc.repository.Demande_de_congerepository;
import com.dpc.repository.Utilisateurrepository;
import com.dpc.service.Utlisateurservice;




@CrossOrigin("*")
@RestController
public class UtilisateurController extends Maincontroller {


	
	@Autowired 
	private Utlisateurservice utilisateurservice ; 	
	
	//fonction ajouter utilisateur
	@PostMapping(value="/ajouterUtilisateur")
		public void saveutilisateur(@RequestBody Utilisateur utilisateur  )
		{
			utilisateurservice.saveutilisateur(utilisateur);
		}
		
		
	
	
	@GetMapping(value="/Afficher tous les utilisateurs")
	public ResponseEntity<List<Utilisateur>>getAlltilisateurs(){
		List<Utilisateur> utilisateurs=utilisateurrepository.findAll();
	return new ResponseEntity<List<Utilisateur>>(utilisateurs,HttpStatus.OK);			
	}
	
		 // fct qui affiche les utilisateurs
/*		  @RequestMapping(value = "/getAllUtilisateur", method = RequestMethod.GET)
		  public Collection<Utilisateur> getUtilisateur() {   
			   return this.utilisateurservice.getUtilisateur();
		  }

	*/		

			 // fct qui affiche les Utilisateur
		   @RequestMapping(value = "/getAllUtilisateurdto", method = RequestMethod.GET)
		   public Collection<UtilisateurDto> getUtilisateurdto() {   
			   return this.utilisateurservice.getUtilisateur().stream().map(utilisateur->convertuserToDto(utilisateur))
		   			.collect(Collectors.toList());

		   }

		 //fct Récupérer un employé par son email
		   
		   
		   
		    @GetMapping(value ="/getUtilisateurbyEmail/{email}") 
		    public Utilisateur afficheutilisateur(@PathVariable String email) {
		        return utilisateurservice.getUtilisateurEmail(email);
		    }
	
		   
		    
		    //fct qui affiche un type a travers son id
		    @GetMapping(value="/getUtilisateurId/{id}")
		    public Utilisateur getUtilisateurId(@PathVariable long id) {
		 	   return utilisateurservice.getUtilisateurId(id);
		 	   
		    }

		    
		    
		    
		    // une fct qui supprime un employes a travers son email
		    @GetMapping(value = "/deleteUserByEmail/{email}")
		    public Utilisateur getUserEmail( String email) {
		    	System.out.println(email);
		    	this.utilisateurservice.deleteEmail(email);

		        return null;
		    }
	
		  //fct pour modifier le contenue de l'entité utilisateur
	        @PostMapping(value = "/updateUtilisateur")
	        public String updateUtilisateur(@RequestBody Utilisateur utilisateur) { //Cette annotation demande à Spring que le JSON contenu dans la partie body de la requête HTTP soit converti en objet Java
	            utilisateurservice.update(utilisateur);
	            return "ok" ; 
	        } 
		    @Autowired
Demande_de_congerepository congerepo ; 
@Autowired
		    Utilisateurrepository utilisateurrepository ; 
 //fct affecter
	/*  
	  @PostMapping(value = "/ajouter utilisateurparrole/{id}")
	  
	  public Utilisateur saveUtilisateur(@RequestBody Utilisateur utilisateur, long id_role) {
		  return this.utilisateurservice.AddEmpToEntrepriseToRole(utilisateur,id_role); 
	  
	  
	  }
	  */
	  }
	  
	 
