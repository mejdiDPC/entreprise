package com.dpc.controller;

import java.util.Collection;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.dpc.controller.Maincontroller;

import com.dpc.domain.Roles;
import com.dpc.domain.Type;
import com.dpc.domain.Utilisateur;
import com.dpc.dto.RolesDto;
import com.dpc.dto.TypeDto;
import com.dpc.dto.UtilisateurDto;
import com.dpc.service.Roleservice;

@RestController
public class RoleController extends Maincontroller{

	
	
	
	
	@Autowired
private Roleservice roleservice ; 

// afficher par nom 	
	@GetMapping(value = "/roles/{name}")
	public ResponseEntity<Roles> findByName(@PathVariable("name") String name) {
	    Roles roles = roleservice.findByName(name);
	    return new ResponseEntity<Roles>(roles, HttpStatus.FOUND);
	}



//fonction ajouter un Role
@PostMapping(value="/ajouterRole")
public void saveRole(@RequestBody Roles  roles)
{
	roleservice.saveRole(roles);
}




// fct qui affiche les Roles
@RequestMapping(value = "/getAllRole", method = RequestMethod.GET)
public Collection<Roles> getRole() {   
  return this.roleservice.getRole();
}

// fct qui affiche les Rolesdto
@RequestMapping(value = "/getAllRoledto", method = RequestMethod.GET)
public Collection<RolesDto> getRoledto() {   
	   return this.roleservice.getRole().stream().map(type->convertRoleToDto(type))
			.collect(Collectors.toList());

}




//fct pour modifier le contenue de l'entité utilisateur
@PostMapping(value = "/updateRoles")
public String updateRole(@RequestBody Roles roles) { //Cette annotation demande à Spring que le JSON contenu dans la partie body de la requête HTTP soit converti en objet Java
    roleservice.update(roles);
    return "ok" ; 
} 

//une fct qui supprime un employes a travers son name
@GetMapping(value = "/deleteRoleByName/{name}")
public Roles getRolesName( String name) {
	System.out.println(name);
	this.roleservice.deleteName(name);

    return null;
}
}
