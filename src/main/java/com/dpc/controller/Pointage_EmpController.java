package com.dpc.controller;

import java.util.Collection;
import java.util.Date;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.dpc.controller.Maincontroller;

import com.dpc.domain.Pointage_Emp;
import com.dpc.domain.Roles;
import com.dpc.domain.Type;
import com.dpc.domain.Utilisateur;
import com.dpc.dto.Pointage_EmpDto;
import com.dpc.dto.UtilisateurDto;
import com.dpc.service.Pointage_Empservice;

@RestController
public class Pointage_EmpController extends Maincontroller {

	@Autowired
	private Pointage_Empservice pointage_Empservice ; 
	
	

	//fonction ajouter pointage_emp
	@PostMapping(value="/ajouterPointage")
	public void savePointage(@RequestBody Pointage_Emp pointage_Emp)
	{
		pointage_Empservice.savePointage(pointage_Emp);
	}
	

	 // fct qui affiche les Pointages
 @RequestMapping(value = "/getAllPointage", method = RequestMethod.GET)
 public Collection<Pointage_Emp> getPointage() {   
	   return this.pointage_Empservice.getPointage();
 }

//fct pour modifier le contenue de l'entité utilisateur
 @GetMapping(value = "/updatePointage")
 public void updatePointage(@RequestBody Pointage_Emp pointage_Emp) { //Cette annotation demande à Spring que le JSON contenu dans la partie body de la requête HTTP soit converti en objet Java
	 pointage_Empservice.update(pointage_Emp);
 } 

	/* // fct qui affiche la liste de pointage
    @RequestMapping(value = "/getAllPointage", method = RequestMethod.GET)
    public Collection<Pointage_EmpDto>getAllPointage(){
    
		return this.pointage_Empservice.getAllPointage().stream().map(pointage_Emp->convertPointageToDto(pointage_Emp))
    			.collect(Collectors.toList());
    }
    */
//une fct qui supprime un pointage a travers son id
@GetMapping(value = "/deletePoitageId/{id}")
public Pointage_Emp getPointageId( long id) {
	System.out.println(id);
	this.pointage_Empservice.deleteId(id);

   return null;
}

//fct qui affiche un Pointage a travers son id
@GetMapping(value="/getPointageId/{id}")
public Pointage_Emp getPointageempId(@PathVariable long id) {
	   return pointage_Empservice.getPointageempId(id);
	   
}
}
