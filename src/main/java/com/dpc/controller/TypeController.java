package com.dpc.controller;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.dpc.controller.Maincontroller;
import com.dpc.domain.Roles;
import com.dpc.domain.Type;
import com.dpc.domain.Utilisateur;
import com.dpc.dto.TypeDto;
import com.dpc.service.Typeservice;



@RestController 
public class TypeController extends Maincontroller {

	@Autowired
	private Typeservice typeservice ; 
	

	//fonction ajouter Type
	@PostMapping(value="/ajouterType")
	public void saveType(@RequestBody Type type )
	{
		typeservice.saveType(type);
	}


	 // fct qui affiche les types
  @RequestMapping(value = "/getAllType", method = RequestMethod.GET)
  public Collection<Type> getType() {   
	   return this.typeservice.getType();
  }

	
	
	
	 // fct qui affiche les types
   @RequestMapping(value = "/getAllTypedto", method = RequestMethod.GET)
   public Collection<TypeDto> getTypedto() {   
	   return this.typeservice.getType().stream().map(type->convertRoleToDto(type))
   			.collect(Collectors.toList());

   }
  

	
	  //fct pour modifier le contenue de l'entité type
   @GetMapping(value = "/typeUpdate")
   public void updateType(@RequestBody Type type) { //Cette annotation demande à Spring que le JSON contenu dans la partie body de la requête HTTP soit converti en objet Java
       typeservice.update(type);
   }
//dct supprimer a travers son type
   @GetMapping(value = "/deleteTypeByType/{type}")
   public Type getTypeType( String type) {
   	System.out.println(type);
   	this.typeservice.deleteType(type);

       return null;
   }
 
   //fct qui affiche un type a travers son id
   @GetMapping(value="/getTypeId/{id}")
   public Type getTypeId(@PathVariable long id) {
	   return typeservice.getTypeId(id);
	   
   }
   
}
