package com.dpc.controller;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dpc.domain.UserDto;
import com.dpc.model.User;
import com.dpc.security.JwtTokenUtil;
import com.dpc.security.JwtUser;
import com.dpc.security.UnauthorizedException;

@CrossOrigin("*")
@RestController
public class AuthenticationController {
	@Value("${jwt.header}")
	private String tokenHeader;
	
	@Autowired
	private AuthenticationManager authenticationManager;
	@Autowired
	private JwtTokenUtil jwtTokenUtil;
	
	@PostMapping(value = "/login")
	public ResponseEntity<UserDto> login (@RequestBody User user, javax.servlet.http.HttpServletRequest request,HttpServletResponse response){
		try {
			Authentication authentication =authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(user.getEmail(), user.getPassword()));
			final JwtUser userDetails =(JwtUser)authentication.getPrincipal();
			SecurityContextHolder.getContext().setAuthentication(authentication);
			final String token = jwtTokenUtil.generateToken(userDetails);
			response.setHeader("Token",token);
			return new ResponseEntity<UserDto>(new UserDto(userDetails.getUser(), token),HttpStatus.OK);
		}catch (Exception e) {
			throw new UnauthorizedException(e.getMessage());
		}
	}
	

}
