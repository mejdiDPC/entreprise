package com.dpc.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.dpc.domain.Demande_de_conge;
import com.dpc.domain.Type;
import com.dpc.domain.Utilisateur;
import com.dpc.dto.UtilisateurDto;
import com.dpc.model.User;

public interface Utilisateurrepository extends JpaRepository<Utilisateur, Long> {

	Utilisateur findByEmail(String email) ;
	Utilisateur findById(long id);
	List<Utilisateur> findAll();


}
