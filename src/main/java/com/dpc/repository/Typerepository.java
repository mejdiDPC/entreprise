package com.dpc.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.dpc.domain.Type;
import com.dpc.dto.TypeDto;

public interface Typerepository extends JpaRepository<Type, Long> {

	Type findByType(String type);

	Type findById(long id);

}
