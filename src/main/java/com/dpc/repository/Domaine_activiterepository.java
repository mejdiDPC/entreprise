package com.dpc.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.dpc.domain.Domaine_activite;

public interface Domaine_activiterepository extends JpaRepository<Domaine_activite,Long> {

	Domaine_activite findByNom(String nom);

}
