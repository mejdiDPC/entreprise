package com.dpc.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.dpc.domain.Type_contrat;

public interface Type_contretrepository extends JpaRepository<Type_contrat, Long> {

	Type_contrat findByType(String type);

}
