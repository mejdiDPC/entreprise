package com.dpc.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.dpc.domain.Entreprise_domaine_activite;

public interface Entreprise_domaine_activiterepository extends JpaRepository<Entreprise_domaine_activite, Long>{

}
