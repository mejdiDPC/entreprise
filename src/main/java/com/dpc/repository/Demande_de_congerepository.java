package com.dpc.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.dpc.domain.Demande_de_conge;
import com.dpc.domain.Utilisateur;

public interface Demande_de_congerepository extends JpaRepository<Demande_de_conge,Long> {

	Demande_de_conge findByMotif(String motif);

	Demande_de_conge findById(long id);

}
 