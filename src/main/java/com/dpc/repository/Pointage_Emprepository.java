package com.dpc.repository;

import java.util.Date;

import org.springframework.data.jpa.repository.JpaRepository;

import com.dpc.domain.Pointage_Emp;
import com.dpc.domain.Type;

public interface Pointage_Emprepository extends JpaRepository<Pointage_Emp,Long> {

	Pointage_Emp findByDate(Date date);

	Pointage_Emp findById(long id);

}
