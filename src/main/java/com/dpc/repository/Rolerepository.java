package com.dpc.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.dpc.domain.Roles;

public interface Rolerepository extends JpaRepository<Roles, Long> {

	Roles findByName(String name);

}
