package com.dpc.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.dpc.domain.Entreprise;
import com.dpc.domain.Utilisateur;

public interface Entrepriserepository extends JpaRepository<Entreprise,Long> {

	Entreprise findByEmail(String email);
}
