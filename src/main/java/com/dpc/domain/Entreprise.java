package com.dpc.domain;

import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Entreprise {
@Id
@GeneratedValue
@Column(unique = true)
	private long id ;





@Column(unique = true)
private String Libelle ;
@Column(unique = true)
private String email ; 
private String adress ; 
private long tel1 ; 
private long tel2 ;
private long faxe ; 
@Column(unique = true)
private String Registre_commerce ; 
@Temporal(TemporalType.DATE)
private Date date_creation ; 
}
