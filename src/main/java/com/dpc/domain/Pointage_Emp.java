package com.dpc.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Pointage_Emp {
	@Id
	@GeneratedValue
    @Column(unique = true)

	private long id ;
	@ManyToOne(fetch=FetchType.EAGER)
	 @JoinColumn(name="id_user",nullable=true)
	 private Utilisateur utilisateur; 
private Date date_entree ; 
private Date date_sortie ; 
@Column(unique = true)
private Date date ;

}
