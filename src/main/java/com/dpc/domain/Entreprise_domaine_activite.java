package com.dpc.domain;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
@NoArgsConstructor
@AllArgsConstructor
@Data
@Entity
public class Entreprise_domaine_activite {

	@Id
@GeneratedValue
	private long id ;
	@ManyToOne(fetch=FetchType.EAGER)
	 @JoinColumn(name="id_entre",nullable=true)
	 private Entreprise entreprise ; 
	
	@ManyToOne(fetch=FetchType.EAGER)
	 @JoinColumn(name="id_domaine",nullable=true)
	 private Domaine_activite dom ; 

}
