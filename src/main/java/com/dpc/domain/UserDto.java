package com.dpc.domain;

import java.io.Serializable;

import com.dpc.model.User;

public class UserDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2390010503065974092L;
	private User user;
	private String token;
	public UserDto(User user, String token) {
		super();
		this.user = user;
		this.token = token;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		
		this.user = user;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	

}
