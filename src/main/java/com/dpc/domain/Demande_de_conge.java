package com.dpc.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
@NoArgsConstructor
@AllArgsConstructor
@Data
@Entity
public class Demande_de_conge {
	
	
	@Id
	@GeneratedValue
	@Column(unique = true)
private long id ; 
	 
    @ManyToOne(fetch=FetchType.EAGER)
   @JoinColumn(name="type_id",nullable=true)
    
    private Type type;

    @ManyToOne(fetch=FetchType.EAGER)
	 @JoinColumn(name="id_user",nullable=true)
	 private Utilisateur utilisateur ;    
    
    
	
	@Column(unique = true)
private Date date_debut;
	@Column(unique = true)
private Date date_fin ; 
	@Column(unique = true)
private Date date_inscription ;
	@Column(unique = true)
private String motif ;
private boolean Validation ;  

}
