package com.dpc.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;
import java.util.stream.Stream;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Utilisateur implements Serializable {

	 @Id 
	 @GeneratedValue
	    @Column(unique = true)
private long id ;
	

	 @ManyToOne(fetch=FetchType.EAGER)
	 @JoinColumn(name="id_role",nullable=true)
	 private Roles roles ; 

	 //@OneToMany(fetch = FetchType.LAZY, mappedBy = "utilisateur")
	 //private Set<Demande_de_conge> demande_de_conges;
	 
	 
	 //@ManyToOne(fetch=FetchType.EAGER)
	 //@JoinColumn(name="id_entre",nullable=true)
	 //private Entreprise entreprise ;
	 
	// @OneToMany(fetch = FetchType.LAZY, mappedBy = "utilisateur")
	// private Set<Pointage_Emp> pointage_Emps;

	 
	 //@ManyToOne(fetch=FetchType.EAGER)
	 //@JoinColumn(name="id_type",nullable=true)
	 //private Type_contrat type_contrat ;

	 @Column(unique = true)
	 private String nom ;
	    @Column(unique = true)
	 private String prenom ; 
	    @Column(unique = true)
	 private String email  ; 
	    private long CIN ; 
	    @Column(unique = true)
	    private long Numero_CNSS ; 
	 private long tel1 ; 
	    private long tel2 ; 
	 private String adresse ;
	 private String situation_familiale ; 
	    @Column(unique = true)
	    @Temporal(TemporalType.DATE)
	   private Date Date_recrutement ; 
	    private boolean enabled ; 
	 private String Username ; 
	 private String password ;
	public Stream<Utilisateur> stream() {
		// TODO Auto-generated method stub
		return null;
	}
	
	
	  

}
