package com.dpc.config;

import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
@Configuration
@EnableWebMvc
@ComponentScan
public class WebMVCconfig extends WebMvcConfigurerAdapter {
	@Bean
	   public ModelMapper modelMapper() {
	       return new ModelMapper();
	   }

	@Override
	   public void addViewControllers(ViewControllerRegistry registry) {
	       registry.addRedirectViewController("/docApi/v2/api-docs", "/v2/api-docs");
	       registry.addRedirectViewController("/docApi/swagger-resources/configuration/ui",
	               "/swagger-resources/configuration/ui");
	       registry.addRedirectViewController("/docApi/swagger-resources/configuration/security",
	               "/swagger-resources/configuration/security");
	       registry.addRedirectViewController("/docApi/swagger-resources", "/swagger-resources");
	   }

	   @Override
	   public void addResourceHandlers(ResourceHandlerRegistry registry) {
	       registry.addResourceHandler("/docApi/swagger-ui.html**")
	               .addResourceLocations("classpath:/META-INF/resources/swagger-ui.html");
	       registry.addResourceHandler("/docApi/webjars/**")
	               .addResourceLocations("classpath:/META-INF/resources/webjars/");

	}
}
