package com.dpc.dto;

import java.io.Serializable;

import lombok.Data;

@Data
public class UtilisateurDto  {
private String nom ;
private String prenom ; 
private String email  ;
private long tel ;

}
