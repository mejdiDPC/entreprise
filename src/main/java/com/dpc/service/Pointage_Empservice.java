package com.dpc.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dpc.domain.Pointage_Emp;
import com.dpc.domain.Roles;
import com.dpc.domain.Type;
import com.dpc.domain.Utilisateur;
import com.dpc.repository.Pointage_Emprepository;

@Service
public class Pointage_Empservice {

	@Autowired
	public Pointage_Emprepository pointage_Emprepository ;
	
	/*//fct qui affiche la liste d'utilisateur
	public List<Pointage_Emp> getAllPointage() {

	    return (List<Pointage_Emp>)pointage_Emprepository.findAll();  
	}
*/


	//fct qui affiche les types
		public List<Pointage_Emp> getPointage() {

		    return (List<Pointage_Emp>)pointage_Emprepository.findAll();  
		}

	//fonction ajout pointage
	public Pointage_Emp savePointage(Pointage_Emp pointage_Emp) {
		this.pointage_Emprepository.save(pointage_Emp);
		return pointage_Emp ; 
	}

	//fct pour modifier le contenue de l'entité utilisateur
	   public Pointage_Emp update(Pointage_Emp pointage_Emp) {

	       pointage_Emprepository.saveAndFlush(pointage_Emp); //ajouter les employes d'une maniere asynchrone
	       return pointage_Emp;
	   }
	// Fct qui supprime Pointage a travers son id
		  public boolean deleteId(long id) {
		        try {

		            Pointage_Emp em = this.pointage_Emprepository.getOne(id);
		            pointage_Emprepository.delete(em);
		            return true;
		        } catch (Exception ex) {
		            return false;
		        }
		    }

		//afciher par id
		  public Pointage_Emp getPointageempId(long id) {
			  return pointage_Emprepository.findById(id);
		  }
}

