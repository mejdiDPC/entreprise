package com.dpc.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;

import com.dpc.domain.Demande_de_conge;
import com.dpc.domain.Roles;
import com.dpc.domain.Type;
import com.dpc.domain.Utilisateur;
import com.dpc.repository.Demande_de_congerepository;

@Service
public class Demande_de_congeservice {

	@Autowired
	public Demande_de_congerepository demande_de_congerepository ; 
	





	//fonction ajout demande de conge
	public Demande_de_conge saveDemande_de_conge(Demande_de_conge demande_de_conge) {
		this.demande_de_congerepository.save(demande_de_conge);
		return demande_de_conge ; 
	}

	//fct qui affiche les demande de conge
	public List<Demande_de_conge> getDemandedeconge() {

	    return (List<Demande_de_conge>)demande_de_congerepository.findAll();  
	}

	//fct pour modifier le contenue de l'entité utilisateur
	   public Demande_de_conge update(Demande_de_conge demande_de_conge) {

	       demande_de_congerepository.saveAndFlush(demande_de_conge); //ajouter les employes d'une maniere asynchrone
	       return demande_de_conge;
	   }

		  
	   // Fct qui supprime Role a travers son nom
		  public boolean deleteMotif(String motif) {
		        try {

		            Demande_de_conge em = this.demande_de_congerepository.findByMotif(motif);
		            demande_de_congerepository.delete(em);
		            return true;
		        } catch (Exception ex) {
		            return false;
		        }
		    }

		//afciher par id
		  public Demande_de_conge getDemandecongeId(long id) {
			  return demande_de_congerepository.findById(id);
		  }

}
