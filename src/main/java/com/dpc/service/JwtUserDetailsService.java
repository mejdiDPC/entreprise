package com.dpc.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.dpc.model.User;
import com.dpc.repository.UserRepository;
import com.dpc.security.JwtUserFactory;
@Service
public class JwtUserDetailsService implements UserDetailsService {

	
	@Autowired private UserRepository userRepository;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User user =userRepository.findByEmailIgnoreCase(username);
		if (user==null) {
			throw new UsernameNotFoundException(String.format("NO User found with usernama '%s'.", username));
		}else {
			return JwtUserFactory.create(user);
		}
		
	}
	
}
