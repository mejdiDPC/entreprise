package com.dpc.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dpc.domain.Roles;
import com.dpc.domain.Type;
import com.dpc.domain.Utilisateur;
import com.dpc.dto.TypeDto;
import com.dpc.repository.Typerepository;


@Service
public class Typeservice {

	@Autowired
	public Typerepository typerepository ; 
	

	//fonction ajout Type
	public Type saveType(Type type) {
		this.typerepository.save(type);
		return type ; 
	}

	//fct qui affiche les types
	public List<Type> getType() {

	    return (List<Type>)typerepository.findAll();  
	}

	//fct pour modifier le contenue de l'entité utilisateur
	   public Type update(Type type) {

	       typerepository.saveAndFlush(type); //ajouter les employes d'une maniere asynchrone
	       return type;
	   }
	   
	// Fct qui supprime Type a travers son type
		  public boolean deleteType(String type) {
		        try {

		            Type em = this.typerepository.findByType(type);
		            typerepository.delete(em);
		            return true;
		        } catch (Exception ex) {
		            return false;
		        }
		    }

//afciher par id
		  public Type getTypeId(long id) {
			  return typerepository.findById(id);
		  }
		  }
	  
