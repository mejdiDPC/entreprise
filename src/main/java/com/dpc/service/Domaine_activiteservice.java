package com.dpc.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dpc.domain.Domaine_activite;
import com.dpc.domain.Type;
import com.dpc.domain.Utilisateur;
import com.dpc.repository.Domaine_activiterepository;

@Service
public class Domaine_activiteservice {

	@Autowired
	private Domaine_activiterepository domaine_activiterepository ; 
	
	

	//fct qui affiche la liste de domaine activité
	public List<Domaine_activite> getAllDomaine() {

	    return (List<Domaine_activite>)domaine_activiterepository.findAll();  
	}



	////fonction ajout Domaine activite
	public Domaine_activite saveDomaine_activite(Domaine_activite domaine_activite) {
		this.domaine_activiterepository.save(domaine_activite);
		return domaine_activite ; 
	}
	//fct pour modifier le contenue de l'entité utilisateur
	   public Domaine_activite update(Domaine_activite domaine_activite) {

	       domaine_activiterepository.saveAndFlush(domaine_activite); //ajouter les employes d'une maniere asynchrone
	       return domaine_activite;
	   }

//fct supprimer domaine a travers le nom
	   public boolean deleteNom(String nom) {
		   try {
			   Domaine_activite domaine=this.domaine_activiterepository.findByNom(nom);
			   domaine_activiterepository.delete(domaine);
			   return true ; 
			   }catch (Exception ex) {
				   return false ; 
			   }
		   
	   }
}
