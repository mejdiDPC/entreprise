package com.dpc.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.dpc.domain.Entreprise;
import com.dpc.domain.Type;
import com.dpc.domain.Utilisateur;
import com.dpc.repository.Entrepriserepository;


@Service
public class Entrepriseservice {

	@Autowired
	public Entrepriserepository entrepriserepository ; 



	//fct qui affiche les entreprises
		public List<Entreprise> getEntreprise() {

		    return (List<Entreprise>)entrepriserepository.findAll();  
		}

	
	
	//fonction ajout entreprise
	public Entreprise saveEntreprise(Entreprise entreprise) {
		this.entrepriserepository.save(entreprise);
		return entreprise ; 
	}

	
	// Fct qui affiche utilisateur a travers son email
@RequestMapping(method=RequestMethod.GET, value="/getEntreprisebyEmail/{email}", produces = MediaType.APPLICATION_JSON_VALUE)
	public Entreprise getEntrepriseEmail(@PathVariable String email) {

	    return entrepriserepository.findByEmail(email);
	}

//une fonction qui supprime entreprise par email 
public boolean deleteEmail(String email) {
    try {

        Entreprise em = this.entrepriserepository.findByEmail(email);
        entrepriserepository.delete(em);
        return true;
    } catch (Exception ex) {
        return false;
    }}
//fct pour modifier le contenue de l'entité utilisateur
public Entreprise update(Entreprise entreprise) {

    entrepriserepository.saveAndFlush(entreprise); //ajouter les employes d'une maniere asynchrone
    return entreprise;
}

}
