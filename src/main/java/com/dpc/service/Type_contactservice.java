package com.dpc.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dpc.domain.Roles;
import com.dpc.domain.Type;
import com.dpc.domain.Type_contrat;
import com.dpc.domain.Utilisateur;
import com.dpc.repository.Type_contretrepository;



@Service
public class Type_contactservice {

	@Autowired
	public Type_contretrepository type_contretrepository ; 
	
	




	// fct qui ajoute un employes
    public Type_contrat saveTypecontrat(Type_contrat type_contrat) {

        this.type_contretrepository.save(type_contrat);
        return type_contrat;
    }
    
    
    


	//fct qui affiche les typesdecontact
	public List<Type_contrat> getTypecontrat() {

	    return (List<Type_contrat>)type_contretrepository.findAll();  
	}
	
	
	
	//fct pour modifier le contenue de l'entité Type_contact
	   public Type_contrat update(Type_contrat type_contrat) {

	       type_contretrepository.saveAndFlush(type_contrat); //ajouter les employes d'une maniere asynchrone
	       return type_contrat;
	   }
	   
	   // Fct qui supprime Type_contret a travers son type
		  public boolean deleteType(String type) {
		        try {

		            Type_contrat em = this.type_contretrepository.findByType(type);
		            type_contretrepository.delete(em);
		            return true;
		        } catch (Exception ex) {
		            return false;
		        }
		    }
}
