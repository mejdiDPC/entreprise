package com.dpc.service;

import java.util.List;

import com.dpc.model.User;

public interface UserService {
	

	User save(User user);

	List<User> findAll();

	User getUserByEmail(String email);

}
