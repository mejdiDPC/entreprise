package Controller;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import Domaine.roles;
import Service.roleservice;
@CrossOrigin("*")
@RestController
public class RoleController  {
@Autowired
public roleservice roleservice ; 


//fct qui affiche les Roles
@RequestMapping(value = "/getAllRole", method = RequestMethod.GET)
public Collection<roles> getRole() {   
return this.roleservice.getRole();
}



}
