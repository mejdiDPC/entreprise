package Domaine;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.springframework.web.bind.annotation.CrossOrigin;


@Entity


public class roles {

	@Id
	@GeneratedValue
	@Column(unique = true)
	private long id; 
	@Column(unique = true)
	private String name ;
	private String description ;
	public roles() {
		super();
	}
	public roles(long id, String name, String description) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	} 


}
